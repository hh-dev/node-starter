-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.23-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for node_starter
CREATE DATABASE IF NOT EXISTS `node_starter` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `node_starter`;

-- Dumping structure for table node_starter.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `url` text COLLATE utf8_bin NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table node_starter.images: ~0 rows (approximately)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT IGNORE INTO `images` (`id`, `width`, `height`, `color`, `description`, `url`, `createdAt`, `updatedAt`) VALUES
	(1, 8192, 6144, '#0A0A0A', NULL, 'https://images.unsplash.com/photo-1535954928789-70499ec4c85a?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=77bfb99bbcd006644ece049451399019', '2018-09-04 15:49:24', '2018-09-04 17:52:40'),
	(2, 5972, 3954, '#D3CB92', NULL, 'https://images.unsplash.com/photo-1535941941053-76ab59cafc1b?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=ee2a8c8d71c6c6aaa4d749012d6b50f5', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(3, 4444, 2986, '#272012', NULL, 'https://images.unsplash.com/photo-1535941863447-76a4dc94e2ba?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=260f21e61aff4938bae9088d4f29aa2c', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(4, 3448, 4592, '#232423', NULL, 'https://images.unsplash.com/photo-1535928968605-9418aa1e574c?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=fea827c56f8ba314e9a5bb4832aa43fe', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(5, 4000, 5000, '#F0C9B0', NULL, 'https://images.unsplash.com/photo-1535923766910-e7143cd1a47c?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=27fea64199053020d63bce63ab55c2ab', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(6, 4896, 3264, '#0C0E13', NULL, 'https://images.unsplash.com/photo-1535909825394-0c9d8f4949f3?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=082c53b01d30689b1bdb73a7398938bd', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(7, 4179, 2934, '#090506', NULL, 'https://images.unsplash.com/photo-1535897690127-a7b9fc4a8893?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=92e05ef20547ebf4d6956da4ffa689a4', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(8, 3965, 5947, '#060708', NULL, 'https://images.unsplash.com/photo-1535892449425-d45bdcb2d016?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=f85cab0a6eba2fb9bf97630612c6c62e', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(9, 7952, 5304, '#C3B597', NULL, 'https://images.unsplash.com/photo-1536061345835-e7f2e53a3b19?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=832d0034e9171d5ce24001d25f673efd', '2018-09-04 15:49:24', '2018-09-04 15:49:24'),
	(10, 5388, 3480, '#FEFEFE', NULL, 'https://images.unsplash.com/photo-1535952019029-12a82236e411?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjM1MTczfQ&s=d70b50fe16ada1d3c55d76129384dd50', '2018-09-04 15:49:24', '2018-09-04 15:49:24');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
