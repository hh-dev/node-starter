# Node Starter

## System requirements
- Node.js 10.x
- NPM 5.x || 6.x
- MySQL 5.7.x

## Project setup
- Clone repository: `git clone git@bitbucket.org:hh-dev/node-starter.git`
- Install dependencies: `npm i`

#### Environment variables
In development, create a file called _.env_ in the project's root directory and add the following variables:

| Variable | Required | Default     | Description                                       |
|----------|----------|-------------|---------------------------------------------------|
| DB_HOST  | Yes      |             | Database host                                     |
| DB_NAME  | Yes      |             | Database name                                     |
| DB_PASS  | Yes      |             | Database user password                            |
| DB_PORT  | Yes      |             | Database port                                     |
| DB_USER  | Yes      |             | Database user name                                |
| DEBUG    | No       | false       | Enable debug logs                                 |
| NODE_ENV | No       | development | Node environment <production, development, test   |
| PORT     | Yes      |             | Port on which to serve the application's API      |

The file will contain the environment variables to be initialized for development. Add environment-specific variables on new lines in the form of `<NAME>=<VALUE>`. See [dotenv](https://www.npmjs.com/package/dotenv) for usage.

#### Import Database
A dump of sample data has been included in the project and can be found at _./mock-data/sample.sql_. To import into MySQL, navigate to the _./mock-data_ directory and run the following command, entering your DB user's password when prompted:
```
mysql -u <username> -p < sample.sql
```

Once complete, a database, _node_starter_ with populated table, _images_ will be created.

## Development
In the _package.json_ file, a convenience script, which uses Gulp has been included to start the app. Simply run 
```
npm start
```

The app should start on the _PORT_ specified in your _.env_ file.
