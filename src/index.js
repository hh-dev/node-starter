const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const handleHealthy = require('./views/routes/healthy');
const handlePhotos = require('./views/routes/photos');
const handleNotFound = require('./views/routes/not-found');
const requestLogger = require('./views/middleware/request-logger');
const configUtils = require('./utils/config');
const db = require('./utils/db');
const logger = require('./utils/logger');

const app = express();

// Init middleware
app.use(requestLogger());
app.use(cors());
app.use(bodyParser.json({ type: 'application/json' }));

// Endpoints
app.use('/healthy', handleHealthy);
app.use('/photos', handlePhotos);

// Handle unknown routes
app.use('/*', handleNotFound);

// Initialize db tables and start app
db.sync()
    .catch((error) => {
        logger.error(error);
        process.exit(1);
    });

const port = configUtils.getApiPort();
app.listen(port, () => logger.info(`Application started on port: ${port}`));
