const router = require('express').Router();
const deleteImage = require('../../controllers/image/delete');
const findAllImages = require('../../controllers/image/find-all');
const findById = require('../../controllers/image/find-by-id');
const saveImage = require('../../controllers/image/save');
const updateImage = require('../../controllers/image/update');
const logger = require('../../utils/logger');
const responseManager = require('../../utils/response-manager');

/**
 * Find one
 * GET /photos/:imageId
 */
router.get('/:imageId', async (req, res) => {
    try {
        const { imageId } = req.params;
        const image = await findById(imageId);

        responseManager.handleSuccess(res, 200, image);
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

/**
 * Find all
 * GET /photos
 */
router.get('/', async (req, res) => {
    try {
        const images = await findAllImages();

        responseManager.handleSuccess(res, 200, images);
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

/**
 * Create
 * POST /photos
 */
router.post('/', async (req, res) => {
    try {
        const data = req.body;
        const image = await saveImage(data);

        responseManager.handleSuccess(res, 201, image);
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

/**
 * Update
 * // PUT /photos/:imageId
 */
router.put('/:imageId', async (req, res) => {
    try {
        const { imageId } = req.params;
        const data = req.body;
        const image = await updateImage(imageId, data);

        responseManager.handleSuccess(res, 200, image);
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

/**
 * Delete
 * // DELETE /photos/:imageId
 */
router.delete('/:imageId', async (req, res) => {
    try {
        const { imageId } = req.params;
        await deleteImage(imageId);

        responseManager.handleSuccess(res, 204);
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

module.exports = router;
