const router = require('express').Router();
const logger = require('../../utils/logger');
const responseManager = require('../../utils/response-manager');

router.get('/', async (req, res) => {
    try {
        responseManager.handleSendData(res, 404, 'Route not found');
    } catch (error) {
        logger.error(error);
        responseManager.handleError(res, 500);
    }
});

module.exports = router;
