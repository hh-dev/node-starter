const { transports, Logger } = require('winston');
const onFinished = require('on-finished');
const configUtils = require('../../utils/config');

const logger = new Logger({
    transports: [
        new transports.Console({
            stringify: true,
            level: configUtils.getSetting('debug') === true ? 'debug' : 'info'
        })
    ]
});

const requestLogger = () => {
    return (req, res, next) => {
        const startTime = process.hrtime();
        const timestamp = (new Date()).toISOString();
        const url = req.url;
        const method = req.method;

        onFinished(res, async () => {
            const responseTime = `${Math.floor(process.hrtime(startTime)[1] / 1000000)}ms`;
            const status = res.statusCode;

            logger.info(`${timestamp} ${status} ${method} ${url} ${responseTime}`);
        });

        next();
    };
};

module.exports = requestLogger;
