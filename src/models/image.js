const Sequelize = require('sequelize');
const db = require('../utils/db');

const { STRING, TEXT, INTEGER } = Sequelize;

const Image = db.define('image', {
    width: { type: INTEGER },
    height: { type: INTEGER },
    color: { type: STRING },
    description: { type: TEXT },
    url: { type: TEXT, isUrl: true, allowNull: false }
});

module.exports = Image;
