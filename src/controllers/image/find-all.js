const _ = require('lodash');
const Image = require('../../models/image');
const logger = require('../../utils/logger');

const findAllImages = async (conditions) => {
    try {
        conditions = _.isPlainObject(conditions) ? conditions : {};
        conditions.raw = true;
        const results = await Image.findAll(conditions);
        return results;
    } catch (error) {
        logger.error(error);
        throw Error;
    }
};

module.exports = findAllImages;
