const Image = require('../../models/image');
const logger = require('../../utils/logger');

const findById = async (id) => {
    try {
        const result = await Image.findById(id);
        return result || {};
    } catch (error) {
        logger.error(error);
        throw Error;
    }
};

module.exports = findById;
