const Image = require('../../models/image');
const logger = require('../../utils/logger');

const update = async (id, data) => {
    try {
        const conditions = {
            where: { id }
        };
        data = _.omit(data, ['id', 'createdAt', 'updatedAt']);
        const image = await Image.update(data, conditions);
        return image;
    } catch (error) {
        logger.error(error);
        throw new Error();
    }
};

module.exports = update;
