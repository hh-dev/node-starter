const Image = require('../../models/image');
const logger = require('../../utils/logger');

const deleteImage = async (id) => {
    try {
        const conditions = {
            where: { id }
        };
        const image = await Image.destroy(conditions);
        return image;
    } catch (error) {
        logger.error(error);
        throw new Error();
    }
};

module.exports = deleteImage;
