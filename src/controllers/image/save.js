const _ = require('lodash');
const Image = require('../../models/image');
const logger = require('../../utils/logger');

const save = async (data) => {
    try {
        data = _.omit(data, ['id', 'createdAt', 'updatedAt']);
        const image = await Image.create(data);
        return image;
    } catch (error) {
        logger.error(error);
        throw new Error();
    }
};

module.exports = save;
