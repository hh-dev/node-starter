const path = require('path');
const { Logger, transports } = require('winston');
const callsite = require('callsite');
const chalk = require('chalk');
const _ = require('lodash');
const configUtils = require('./config');

const logger = new Logger({
    transports: [
        new transports.Console({
            prettyPrint: true,
            stringify: true,
            humanReadableUnhandledException: true,
            level: configUtils.getSetting('app.debug') === true ? 'debug' : 'info'
        })
    ]
});

const getLogContext = (site) => {
    const excludeSysFilepath = path.join(__dirname, '../../');
    let fileName = _.isString(site.getFileName()) ? site.getFileName() : '';
    fileName = fileName.replace(excludeSysFilepath, '');

    const functionName = _.isString(site.getFunctionName()) ?
        `${site.getFunctionName().replace(/.then/g, '').replace(/.catch/g, '')}` : 'anonymous';

    return `${fileName}#${functionName}, ln ${site.getLineNumber()}`;
};

const logMessage = (level, msg, delta) => {
    const siteDelta = _.isInteger(delta) ? 2 + delta : 2;
    const site = _.get(callsite(), [siteDelta]);
    const context = getLogContext(site);

    const color = { debug: 'cyan', info: 'white', warn: 'yellow', error: 'red' };
    logger[level](`${chalk[color[level]](msg)}${context ? chalk.gray(` > ${context}`) : ''}`);
};

module.exports = {
    debug: (msg, siteDelta) => logMessage('debug', msg, siteDelta),
    info: (msg, siteDelta) => logMessage('info', msg, siteDelta),
    warn: (msg, siteDelta) => logMessage('warn', msg, siteDelta),
    error: (msg, siteDelta) => logMessage('error', msg, siteDelta)
};
