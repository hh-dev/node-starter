const _ = require('lodash');
const settings = require('../settings');

const getSetting = (path) => _.get(settings, path);

const getApiPort = () => _.get(settings, 'api.port');

module.exports = {
    getSetting,
    getApiPort
};
