const Sequelize = require('sequelize');
const configUtils = require('./config');

const { host, name, pass, port, user } = configUtils.getSetting('db');

const options = {
    host,
    port,
    dialect: 'mysql',
    pool: { max: 5, min: 0, acquire: 30000, idle: 10000 },
    operatorsAliases: false,
    logging: false
};

const db = new Sequelize(name, user, pass, options);

module.exports = db;
