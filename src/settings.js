const settings = {
    app: {
        environment: process.env.NODE_ENV,
        debug: process.env.DEBUG === 'true'
    },
    api: {
        port: process.env.PORT
    },
    db: {
        host: process.env.DB_HOST,
        name: process.env.DB_NAME,
        pass: process.env.DB_PASS,
        port: process.env.DB_PORT,
        user: process.env.DB_USER
    }
};

module.exports = settings;
