FROM node:10-alpine

COPY ./src/ /app/src/
COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

WORKDIR /app

RUN NODE_ENV=production npm install --warn

CMD node src

EXPOSE 3000
